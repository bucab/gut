import gzip
import os
import pandas
from pathlib import Path
import pytest
import random
from shutil import rmtree
from tempfile import NamedTemporaryFile, TemporaryDirectory
import time

test_scc = False
if test_scc :
    scratch_dir = '/net/scc4/scratch'
else :
    scratch_dir = None

class RandomFASTQ(object) :
    def __init__(self,n=100,l=50,gz=False) :
        self.f = f = NamedTemporaryFile(
                'wt',
                delete=False,
                suffix='.fq.gz' if gz else '.fq',
                dir=scratch_dir
        )
        self.path = f.name
        f.close()

        if gz :
            f = gzip.open(self.path,'wt')
        else :
            f = open(self.path,'wt')

        # write n fake sequences
        for i in range(n) :
            f.write('@seq{}\n'.format(i))
            f.write(''.join(random.choices(list('ACGT'),k=l))+'\n')
            f.write('+\n')
            f.write(''.join(random.choices(list(';<=>?@ABCDEFGHI'),k=l))+'\n')
        f.close()
    def cleanup(self) :
        os.unlink(self.path)
    def __enter__(self) :
        return self
    def __exit__(self,type,value,traceback) :
        self.cleanup()

class RandomCSV(object):
    def __init__(self,n=100,l=50) :
        self.f = f = NamedTemporaryFile('wt',delete=False,suffix='.csv',dir=scratch_dir)
        self.path = f.name
        # write n fake sequences
        f.write(''.join(random.choices(list('abcdefg'),k=50)))
        f.close()
    def cleanup(self) :
        os.unlink(self.path)
    def __enter__(self) :
        return self
    def __exit__(self,type,value,traceback) :
        self.cleanup()

class RandomSeqAndReads(object) :
    def __init__(self, seq_len=10000, num_seq=100, read_len=50, mate_dist=50) :

        # the 'reference' sequence
        ref = random.choices(list('ACGT'),k=seq_len)

        # a random sequence of ACGT
        self._fa = fa = NamedTemporaryFile('wt',delete=False,suffix='.fa',dir=scratch_dir)
        fa.write('>random\n{}\n'.format(
            ''.join(ref)
        ))
        fa.close()
        self.fa = fa.name

        self._read1 = read1 = NamedTemporaryFile('wt',delete=False,suffix='.fq',dir=scratch_dir)
        self.read1 = read1.name

        self._read2 = read2 = NamedTemporaryFile('wt',delete=False,suffix='.fq',dir=scratch_dir)
        self.read2 = read2.name

        rev_comp = str.maketrans('ACGT','TGCA')

        totlen = read_len*2+mate_dist
        for i,p in enumerate(random.choices(list(range(seq_len-totlen)),k=num_seq)) :
            mate_start = p+read_len+mate_dist
            seq1 = ''.join(ref[p:p+read_len])
            seq2 = ''.join(ref[mate_start:mate_start+mate_dist])

            qual = ''.join(random.choices(list(';<=>?@ABCDEFGHI'),k=read_len))

            read1.write('@seq{}:1\n'.format(i))
            read1.write('{}\n'.format(seq1))
            read1.write('+\n')
            read1.write(qual+'\n')

            # read 2 is reverse complemented from ref
            read2.write('@seq{}:2\n'.format(i))
            read2.write('{}\n'.format(seq2.translate(rev_comp)[::-1]))
            read2.write('+\n')
            read2.write(qual+'\n')

        read1.close()
        read2.close()

    def cleanup(self) :
        os.unlink(self.fa)
        os.unlink(self.read1)
        os.unlink(self.read2)
    def __enter__(self):
        return self
    def __exit__(self,type,value,traceback) :
        self.cleanup()

@pytest.fixture
def sample_info() :
    df = pandas.DataFrame(
        [
            ['A','brainz','mousey','RNA','blue',3],
            ['B','brainz','mousey','RNA','blue',4],
            ['C','brainz','mousey','RNA','red',5],
            #['D','brainz','mousey','RNA','red',6],
        ],
        columns=(
            'Sample name',
            'source name',
            'organism',
            'molecule',
            'trait1',
            'trait2',
        )
    )
    return df

# we create a bunch of temporary files in this fixture
@pytest.fixture(scope='module')
def file_info() :
    a_raw = RandomSeqAndReads()
    a_proc = RandomCSV()

    b_raw = RandomFASTQ()
    b_proc = RandomCSV()

    c_raw = RandomFASTQ(gz=True)
    c_proc = RandomCSV()

    all_proc = RandomCSV()

    df = pandas.DataFrame(
        [
            ['A','PE fastq',a_raw.read1,'fastq','Illumina HellaSeq','1'],
            ['A','PE fastq',a_raw.read2,'fastq','Illumina HellaSeq','2'],
            ['A','csv',a_proc.path,'sample counts','',''],
            ['B','SE fastq',b_raw.path,'fastq','Illumina HellaSeq','1'],
            ['B','csv',b_proc.path,'sample counts','',''],
            ['B','csv',all_proc.path,'raw counts matrix',''],
            ['C','SE fastq',c_raw.path,'fastq','Illumina HellaSeq','1'],
            ['C','csv',c_proc.path,'sample counts','',''],
            ['C','csv',all_proc.path,'raw counts matrix','',''],
        ],
        columns=(
            'Sample name',
            'rectype',
            'path',
            'file type',
            'instrument model',
            'end'
        )
    )
    yield df

    for x in (a_raw, a_proc, b_raw, b_proc, c_raw, c_proc, all_proc) :
        x.cleanup()

def test_GEODataset(sample_info, file_info) :
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        d = GEODataset(tmpd,sample_info,file_info)
        with NamedTemporaryFile('wt',dir=scratch_dir) as f :
            f.write('some stuff')
            f.flush()

            d.stage_file(f.name)
            assert os.path.exists(os.path.join(tmpd,f.name))

            checksum = d.md5(f.name)
            md5_fn = os.path.join(tmpd,'.cache',os.path.basename(f.name)+'.md5')
            assert os.path.exists(md5_fn)
            assert checksum == 'beb6a43adfb950ec6f82ceed19beee21'

        assert 'file name' in d.file_info

def test_validate(sample_info, file_info) :
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        d = GEODataset(tmpd, sample_info, file_info)

        d.validate()

    # check that error is raised when a sample is missing from sample info
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info.drop(index=[2]), file_info)

    # check that error is raised when a sample is missing from file info
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info, file_info.drop(index=[0,1]))

def test_validate_sample_info(sample_info, file_info) :
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        d = GEODataset(tmpd, sample_info, file_info)
        d.validate_sample_info()

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        sample_info_blank = sample_info.copy()
        sample_info_blank.loc[0,'Sample name'] = ''

        # sample name cannot be blank
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info_blank, file_info)

    # check for missing required column
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info.drop(columns=['Sample name']), file_info)

def test_validate_file_info(sample_info, file_info) :
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        d = GEODataset(tmpd, sample_info, file_info)
        d.validate_file_info()

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        file_info_blank = file_info.copy()
        file_info_blank.loc[0,'Sample name'] = ''
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info, file_info_blank)

    # check for missing required column
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info, file_info.drop(columns=['Sample name']))

    # check for missing PE record
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        bad_pe = file_info.copy().drop(index=[0])
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info, bad_pe)

    # check for incorrect PE record end
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        bad_pe = file_info.copy()
        bad_pe.loc[0,'end'] = '2'
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info, bad_pe)

    # all samples must have both raw and processed files
    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        with pytest.raises(ValidationError) :
            d = GEODataset(tmpd, sample_info, file_info.drop(index=[2]))


def test_sample_section(sample_info,file_info) :
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :
        d = GEODataset(tmpd, sample_info, file_info)
        df = d.samples_section

        assert df.loc[0,'Sample name'] == 'A'

def test_processed_files_section(sample_info,file_info) :
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :

        sample_info_subset = sample_info[sample_info['Sample name'] == 'A'].copy()
        file_info_subset = file_info[file_info['Sample name'] == 'A'].copy()

        with NamedTemporaryFile('wt',dir=scratch_dir) as f :
            f.write('some stuff')
            f.flush()

            file_info_subset.loc[2,'path'] = f.name

            d = GEODataset(tmpd, sample_info_subset, file_info_subset)
            section = d.processed_files_section
            assert section.shape[0] == 1
            assert section.loc[2,'file checksum'] == 'beb6a43adfb950ec6f82ceed19beee21'

        # snakemake should have created a .snakemake directory in the
        # output directory
        snakemake_dir = Path(tmpd) / '.snakemake'
        assert snakemake_dir.exists()

        # the .snakemake persistence directory can sometimes contain files,
        # preventing deletion of the temporary directory, delete it manually
        #rmtree(snakemake_dir / 'log',ignore_errors=True)

def test_raw_file_section(sample_info,file_info) :

    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :

        sample_info_subset = sample_info[sample_info['Sample name'] == 'B'].copy()
        file_info_subset = file_info[file_info['Sample name'] == 'B'].copy()

        with RandomFASTQ() as f :
            file_info_subset.loc[3,'path'] = f.path

            d = GEODataset(tmpd, sample_info_subset, file_info_subset)

            assert d.raw_files_section.loc[3,'read length'] == '50'

        # snakemake should have created a .snakemake directory in the
        # output directory
        snakemake_dir = Path(tmpd) / '.snakemake'
        assert snakemake_dir.exists()

        # the .snakemake persistence directory can sometimes contain files,
        # preventing deletion of the temporary directory, delete it manually
        #rmtree(snakemake_dir,ignore_errors=True)

def test_paired_file_section(sample_info,file_info):

    from geo_upload_tool import GEODataset, ValidationError, main

    snakemake_opts={'cores':2,'printshellcmds':True,'printreason':True}
    if test_scc :
        snakemake_opts['cluster'] = 'qsub -P bubhub -pe omp {threads}'

    with TemporaryDirectory(dir=scratch_dir) as tmpd :

        sample_info_subset = sample_info[sample_info['Sample name'] == 'A'].copy()
        file_info_subset = file_info[file_info['Sample name'] == 'A'].copy()

        with RandomSeqAndReads() as f :
            file_info_subset.loc[0,'path'] = f.read1
            file_info_subset.loc[1,'path'] = f.read2
            d = GEODataset(tmpd, sample_info_subset, file_info_subset, ref_fa=f.fa,
                    snakemake_opts=snakemake_opts)

            pe = d.paired_end_section

            assert pe.shape[0] == 1
            assert pe['average insert size'].iloc[0] == 150
            assert pe['standard deviation'].iloc[0] == 0

        # should still work if ref_fa is defined on each file info record
        with RandomSeqAndReads() as f :
            file_info_subset.loc[0,'path'] = f.read1
            file_info_subset.loc[1,'path'] = f.read2
            file_info_ref_fa = file_info_subset.copy()
            file_info_ref_fa['ref_fa'] = f.fa
            d = GEODataset(tmpd, sample_info_subset, file_info_ref_fa,
                    snakemake_opts=snakemake_opts)

            pe = d.paired_end_section
            assert pe.shape[0] == 1
            assert pe['average insert size'].iloc[0] == 150
            assert pe['standard deviation'].iloc[0] == 0

        # with neither constructor nor file_info ref_fa, insert size should be blank
        with RandomSeqAndReads() as f :
            file_info_subset.loc[0,'path'] = f.read1
            file_info_subset.loc[1,'path'] = f.read2
            d = GEODataset(tmpd, sample_info_subset, file_info_subset,
                    snakemake_opts=snakemake_opts)

            pe = d.paired_end_section
            assert pe.shape[0] == 1
            assert pe['average insert size'].iloc[0] == ''
            assert pe['standard deviation'].iloc[0] == ''

        # should be able to handle gzipped fasta files
        with RandomSeqAndReads() as f :
            file_info_subset.loc[0,'path'] = f.read1
            file_info_subset.loc[1,'path'] = f.read2

            ref_fa_gz = f.fa+'.gz'

            with gzip.open(ref_fa_gz,'wt') as gz_f :
                with open(f.fa,'rt') as fa_f :
                    gz_f.write(fa_f.read())

            d = GEODataset(tmpd, sample_info_subset, file_info_subset, ref_fa=ref_fa_gz,
                    snakemake_opts=snakemake_opts)

            pe = d.paired_end_section

            assert os.path.exists(os.path.join(tmpd,'.cache/A_Aligned.out.bam'))
            assert os.path.exists(os.path.join(tmpd,'.cache/A_Aligned.out.bam_imdstats.csv'))

            assert pe.shape[0] == 1
            assert pe['average insert size'].iloc[0] == 150
            assert pe['standard deviation'].iloc[0] == 0

        # snakemake should have created a .snakemake directory in the
        # output directory
        snakemake_dir = Path(tmpd) / '.snakemake'
        assert snakemake_dir.exists()

        # the .snakemake persistence directory can sometimes contain files,
        # preventing deletion of the temporary directory, delete it manually
        #rmtree(snakemake_dir,ignore_errors=True)

def test_main(sample_info,file_info) :
    from geo_upload_tool import main

    with NamedTemporaryFile('wt',dir=scratch_dir) as sample_f :
        with NamedTemporaryFile('wt',dir=scratch_dir) as file_f :
            sample_info.to_csv(sample_f,index=False)
            sample_f.flush()

            file_info.to_csv(file_f,index=False)
            file_f.flush()

            main(['validate',sample_f.name,file_f.name])

            main(['build',sample_f.name,file_f.name])

            if test_scc :
                # cluster utilization
                main(['build',sample_f.name,file_f.name,
                    '--cores', 2,
                    '--cluster','qsub -P bubhub -pe omp {threads}'
                    ])


# the big kahuna
def test_process(sample_info,file_info):
    from geo_upload_tool import GEODataset, ValidationError

    with TemporaryDirectory(dir=scratch_dir) as tmpd :

        d = GEODataset(tmpd, sample_info, file_info)
        metadata = d.process()

        # snakemake should have created a .snakemake directory in the
        # output directory
        assert os.path.exists(os.path.join(tmpd,'.snakemake'))

