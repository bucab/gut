conda install -c conda-forge -c bioconda samtools cython STAR
pip install snakemake docopt pandas pysam openpyxl setuptools future pytest twine
